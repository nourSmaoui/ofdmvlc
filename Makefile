#CPPFLAGS=-g $(shell root-config --cflags) -std=c++0x
CPPFLAGS+= -I /usr/local/include
#LDFLAGS=-g $(shell root-config --ldflags)
#~ LDLIBS=$(shell root-config --libs)
LDLIBS=-L/usr/lib/x86_64-linux-gnu -L/usr/local/lib -lfftw3 -lrt -lm

SRCS= main.c functions.c
OBJS=$(subst .c,.o,$(SRCS))

main: $(OBJS)
	gcc $(LDFLAGS) -o main main.o functions.o $(LDLIBS) 

main.o: main.c functions.c functions.h
	gcc $(CPPFLAGS) -c main.c
    
functions.o: functions.c
	gcc $(CPPFLAGS) -c functions.c
	
clean:

	rm *.o
