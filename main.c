	#include "functions.h"
#include <stdio.h>
#include <stdlib.h>
#include <fftw3.h>
#include <time.h>


int main(int argc, char **argv)
{
	int ret = 0;
	uint8_t* data = NULL;
	
	long bLength;
	int nSymbols;
	fftw_complex *mod;
	fftw_complex *ifft;
	fftw_complex *signal;
	fftw_complex *levels;
	struct timespec start_time,stop_time;
	
	
	
	mod = fftw_alloc_complex(SIZE_IFFT);
	ifft = fftw_alloc_complex(SIZE_IFFT);
	// read from file
	bLength = OFDM_readFile(argv[1], &data);
	
	if(bLength)
	{
		//printf("file size is %ld \n",bLength);
	}
	else if(bLength == OFDM_FileError)
	{
		printf("file error \n");
		return 0;
	}
	else if(bLength == OFDM_AllocationError)
	{
		printf("allocation error \n");
		return 0;
	}
	
	
	nSymbols = (((bLength*8)%N_REAL_SUBCARRIERS) == 0 ?((bLength*8)/N_REAL_SUBCARRIERS): ((bLength*8)/N_REAL_SUBCARRIERS) +1 );
	signal = fftw_alloc_complex(SIZE_IFFT*nSymbols);
	levels = fftw_alloc_complex(SIZE_IFFT*nSymbols);
	//printf("number of sybols is %d\n",nSymbols);
	//printf("the first byte is %02x\n",data[100]);
	int i = 0;
	int size;
	//start timer
	
	clock_gettime(CLOCK_MONOTONIC, &start_time);
	
	for (i=0; i<nSymbols; i++)
	{
		size = 0;
		size = ((i+1)*3 <= bLength ? 3 : bLength-i*3);
		//printf("%d:the size is %d\n",i,size);
		modulate(data,size,i*3, mod);
		/*int k = 0;
		for(k = 0 ; k < 64 ; k++)
		{
			printf("%d: mod %lf\n",k, mod[k][0]);

		}*/
		//ifft
		performIfft(mod,ifft);
		int k = 0;
		for(k = 0 ; k < 64 ; k++)
		{
			printf("%d: real %lf im %lf\n",k, ifft[k][0],ifft[k][1]);
		}
		
		
		sum_samples(signal, ifft, 64, i*64);
		
		
	}
	//stop timer
	
	clock_gettime(CLOCK_MONOTONIC, &stop_time);
	
	printf("The time took for a buffer of length %d this operation is %ld \n ", bLength, (stop_time.tv_sec - start_time.tv_sec)*1000000000+stop_time.tv_nsec - start_time.tv_nsec);
	
	int j = 0;
	double minimum = signal[0][0];
	double max = signal[0][0];
	for(j = 1 ; j < SIZE_IFFT* nSymbols; j++)
	{
		if(minimum > signal[j][0])
		minimum = signal[j][0];

		if(max < signal[j][0])
		max= signal[j][0];
	}
	printf("minimum is %lf\n",minimum);
	
	if(minimum < 0)
	{
		for(j = 0; j < SIZE_IFFT* nSymbols; j++)
		{
			signal[j][0] -= minimum;
			printf("%lf\n",signal[j][0]);
		}
		max-=minimum;
		printf("the max is %lf",max);
	}
	
	
	printf("initializing gpios \n");
	ret = init_gpios();
	if(ret < 0)
	{
		printf("init error");
		return 0;
	}
	//uint32_t pin = 1 << 7;
    	convert_signal_to_levels(signal, levels, max,SIZE_IFFT*nSymbols);
    	while(1)
	{
	int s_cpt=0;
    	for(s_cpt=0;s_cpt<SIZE_IFFT*nSymbols;s_cpt++)
	{
        // set cs to low
        clear_gpio(cs);
        
        // send high byte
        uint16_t buffer = levels[s_cpt][0] ;
	buffer = 0x0FFF & buffer;
	buffer = 0x3000 | buffer;
	SPI_write(buffer);
        
        // set cs to low
        set_gpio(cs);
	delay_n_NOP();
	}
	}
	
/*	while(1)
	{
		set_gpio(pin_map[4]);
		clear_gpio(pin_map[4]);
	}

*/
	//~ set_gpio(pin_map[0]);
	//~ set_gpio(pin_map[1]);
	//~ set_gpio(pin_map[2]);
	//~ set_gpio(pin_map[3]);
	//~ set_gpio(pin_map[4]);
	//~ set_gpio(pin_map[5]);
	//~ set_gpio(pin_map[6]);
	//~ set_gpio(pin_map[7]);
	//~ set_gpio(pin_map[8]);
	//~ set_gpio(pin_map[9]);
//	clear_gpio(pin_map[0]);
/*	clear_gpio(pin_map[2]);
	clear_gpio(pin_map[3]);
	clear_gpio(pin_map[4]);
	clear_gpio(pin_map[5]);
	clear_gpio(pin_map[6]);
	clear_gpio(pin_map[7]);
	clear_gpio(pin_map[8]);
	clear_gpio(pin_map[9]);
	set_gpio(pin_map[10]);
*/	
	fftw_free(mod);
	fftw_free(ifft);
	fftw_free(signal);
	return 0;
}
